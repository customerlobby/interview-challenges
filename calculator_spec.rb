require_relative 'calculator'
require 'rspec'

describe NumberProcessor do

  let(:calculator) { NumberProcessor.new(1,2,3,4) }
  
  describe '#add_number_one_to_another(b,c)' do
    it 'returns the right value' do
      expect(NumberProcessor.add_number_one_to_another(5,6)).to eq(30)
    end
  end

  describe 'multiplication_of_numbers(k,l)' do
    it 'returns the product of the first value and the second value' do
      expect(NumberProcessor.multiplication_of_numbers('1', 2)).to eq(2)
    end
  end

  describe '#subtractions' do
    it 'multiplies the first and second number by the third' do
      expect(NumberProcessor.subtractions(1,3)).to eq(-2)
    end
  end
end